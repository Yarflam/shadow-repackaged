# Shadow Repackaged

## Introduction

This is Shadow AppImages repackaged with some adjustments / tuning / improvements to support more distributions (Ubuntu 21.10+, Manjaro, Arch) until it's fixed by Shadow !

## Content

 - AppRun removal: AppRun is legacy tech and has been removed) - https://docs.appimage.org/introduction/software-overview.html
 - Extra libs removal: This is part1 of cleaning process to ensure better compatibility with newer/"rolling release" distributions 
 - Possible improvements / bash script changes

## Known Issues

 - Auto Updates is not implementedi yet, which means in worst case scenario, current modified AppImage will be replaced by official and you need to download newer repackage from here
 - [Shadow] Touchpad gesture not working (2 fingers scrolling) with local Linux user in "input" group (LIBINPUT)
 - [Shadow] Difficulties to use Shadow hotkeys with local Linux user in "input" group (LIBINPUT)
 - [Shadow] Mouse jittering, Keyboard lost/ignored keys with local Linux user **not** in "input" group (LEGACY, see **Installation/Usage**)
 - [Shadow] Issues with dual+ display local Linux
 - [Shadow] Won't authenticate on SSO if a local **Plex** server is running (Workaround: stop **Plex**, restart Shadow Launcher and authenticate)
 - [Shadow] If run as **root**/**sudo**, can't authenticate on Shadow SSO
 - [Shadow] Browser is not launching to authenticate on Shadow's SSO (Workaround: ensure default browser is configured `sudo update-alternatives --config x-www-browser`, shadow launcher uses **xdg-open** using your browser make also sure `xdg-open https://shadow.tech` opens a web browser.
 - [Shadow] Stream is colored in RED/PINK (see: https://nicolasguilloux.github.io/blade-shadow-beta/issues#the-drirc-fix)
 - [Shadow] Using HD3000-4600 stream is half displayed with green at bottom (use Ubuntu 18.04 or Shadowcker (https://gitlab.com/aar642/shadowcker))
 - [Shadow] Using Intel iGPU, Stream is not opening or black window is displayed (if using Ubuntu 21.04+ `sudo apt install intel-media-va-driver-non-free` and/or start Shadow using `LIBVA_DRIVER_NAME=iHD ./Shadow*.AppImage`)
 - [Shadow] Using an NVIDIA GPU it won't work (fragile workaround is to install official NVIDIA driver, libva-vdpau (https://gitlab.com/aar642/libva-vdpau-driver) and ensure `vainfo` yells results)
 - ~~[Shadow] (X)Wayland not supported, you need to use X11/Xorg (`echo $XDG_SESSION_TYPE` should return **X11** or **tty** if you are using i3~~ (Wayland support is in Alpha Client now for testing)
 - [Shadow] There's no sound/sound is low/only output from one speaker (you need to configure Shadow's Windows to **stereo** AND restart stream)
 - [Shadow] Intel XE iGPU are not supported with Ubuntu 18.04
 - [Shadow] Using Alpha & Wayland, there's no QuickMenu
 - [Shadow] USB over IP (USB Forward) is not implemented in Linux client
 - [Shadow] R:0x7F indicates missing lib to start Shadow Renderer, install missing libs package based on results of (ALL **except** libcef/sentry)
 - [Shadow] Evaluate usage of libglvnd for more advanced usage, docker, other vendor (https://github.com/NVIDIA/libglvnd)
 ```bash
  cd /tmp/ && wget "https://gitlab.com/aar642/shadow-repackaged/-/jobs/artifacts/main/raw/Shadow-x86_64.AppImage?job=build" -O Shadow-x86_64.AppImage && chmod a+x Shadow-x86_64.AppImage && ./Shadow-x86_64.AppImage --appimage-extract && cd squashfs-root && LANG=en_US.UTF-8 find . -name '*.so*' -exec ldd {} + |grep 'not found' && LANG=en_US.UTF-8 find . -type f -executable -exec ldd {} + |grep 'not found'
 ```

## Installation / Usage

It is used like official AppImage, shouldn't be run as **root** or **sudo** and may requires **--no-sandbox** parameter or you may not be able to launch or authenticate on Shadow's SSO and add your local linux user to **input** group (`sudo usermod -aG input $USER`):

### Stable
```bash
wget "https://gitlab.com/aar642/shadow-repackaged/-/jobs/artifacts/main/raw/Shadow-x86_64.AppImage?job=build" -O Shadow-x86_64.AppImage
chmod a+x Shadow-x86_64.AppImage
./Shadow-x86_64.AppImage
```

### Beta
```bash
wget "https://gitlab.com/aar642/shadow-repackaged/-/jobs/artifacts/main/raw/Shadow_Beta-x86_64.AppImage?job=build" -O Shadow_Beta-x86_64.AppImage
chmod a+x Shadow_Beta-x86_64.AppImage
./Shadow_Beta-x86_64.AppImage
```

### Alpha
```bash
wget "https://gitlab.com/aar642/shadow-repackaged/-/jobs/artifacts/main/raw/Shadow_Alpha-x86_64.AppImage?job=build" -O Shadow_Alpha-x86_64.AppImage
chmod a+x Shadow_Alpha-x86_64.AppImage
./Shadow_Alpha-x86_64.AppImage
```

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
[![BuyMeCoffee][buymecoffeebadge]][buymecoffee]

## Disclaimer

This is a community project, project is not affliated to Shadow in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Shadow](http://www.shadow.tech/).

[buymecoffee]: https://www.buymeacoffee.com/latqazuzw
[buymecoffeebadge]: https://img.shields.io/badge/buy%20me%20a%20coffee-donate-yellow?style=for-the-badge
